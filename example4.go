package main

import (
//	"bytes"
//	"database/sql"
//	"fmt"
//	"net/http"
//	"gopkg.in/gin-gonic/gin.v1"
	_"github.com/jinzhu/gorm/dialects/mysql"
	"github.com/jinzhu/gorm"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
	"fmt"
	"strconv"
)
type Users struct {
	gorm.Model
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
}
type TransparentUsers struct {
	Id uint `json:"id"`
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
}
func main()  {
	db:=InitDB()
	db.AutoMigrate(&Users{})//--> This will create a table name is "users"
	gin:=gin.Default()
	gin.Use(Cors())
	v1:=gin.Group("/api/v1")
	{
		v1.POST("/users",CreateUser)
		v1.GET("/users",GetUsers)
		v1.GET("/users/:id",GetUser)
		v1.PUT("/users/:id",UpdateUser)
		v1.DELETE("/users/:id",DeleteUser)
	}
	gin.Run(":8080")
}
func InitDB() *gorm.DB{
	db,err:=gorm.Open("mysql","root:123456a@@/gotest?charset=utf8&parseTime=True&loc=Local")
	if err !=nil{
		panic(err)
	}
	return db
}
func Cors() gin.HandlerFunc{
	return func(c *gin.Context) {
		c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}
func GetUsers(c *gin.Context)  {
	db:=InitDB()
	defer db.Close()
	var users []Users
	var _users []TransparentUsers
	db.Find(&users)
	if len(users) <=0{
		c.JSON(http.StatusNotFound,gin.H{
			"status": http.StatusNotFound,
			"message": "No users found",
		})
		return
	}
	for _,user:= range users{
		_users=append(_users,TransparentUsers{Id:user.ID,FirstName:user.FirstName,LastName:user.LastName})
	}
	c.JSON(http.StatusOK,gin.H{
		"status": http.StatusOK,
		"data": _users,
	})
}
func CreateUser(c *gin.Context){
	db:=InitDB()
	defer db.Close()
	var user Users
	c.Bind(&user)
	if user.FirstName != "" && user.LastName != ""{
		db.Save(&user)
		msg:=fmt.Sprintf("User was created successfully.Firtname: %s.Lastname: %s",user.FirstName,user.LastName)
		c.JSON(http.StatusOK,gin.H{
			"status": http.StatusOK,
			"message": msg,
		})
		return
	}
	c.JSON(http.StatusConflict,gin.H{
		"status": http.StatusConflict,
		"message": "Fields is empty",
	})
}
func GetUser(c *gin.Context){
	db:=InitDB()
	defer db.Close()
	id:=c.Params.ByName("id")
	var user Users
	db.First(&user,id)
	if user.ID == 0{
		c.JSON(http.StatusNotFound,gin.H{
			"status": http.StatusNotFound,
			"message": "No user found",
		})
		return
	}
	_user:=TransparentUsers{Id:user.ID,FirstName:user.FirstName,LastName:user.LastName}
	c.JSON(http.StatusOK,gin.H{
		"status": http.StatusOK,
		"data": _user,
	})
}
func UpdateUser(c *gin.Context)  {
	db:=InitDB()
	defer db.Close()
	id:=c.Params.ByName("id")
	var user Users
	db.First(&user,id)
	if user.ID ==0{
		c.JSON(http.StatusNotFound,gin.H{
			"status": http.StatusNotFound,
			"message": "No user found",
		})
		return
	}
	var newuser Users
	c.Bind(&newuser)
	if newuser.FirstName != "" && newuser.LastName != ""{
		db.Model(&user).Update("first_name",newuser.FirstName)
		db.Model(&user).Update("last_name",newuser.LastName)
		db.Save(&user)
		c.JSON(http.StatusOK,gin.H{
			"status": http.StatusOK,
			"message": "User #"+strconv.Itoa(int(user.ID))+ " was updated succesfully",
		})
	}
}
func DeleteUser(c *gin.Context){
	db:=InitDB()
	defer db.Close()
	id:=c.Params.ByName("id")
	var user Users
	db.First(&user,id)
	if user.ID == 0{
		c.JSON(http.StatusNotFound,gin.H{
			"status":http.StatusNotFound,
			"message": "No user found",
		})
		return
	}
	db.Delete(&user)
	c.JSON(http.StatusOK,gin.H{
		"status": http.StatusOK,
		"message": "User id #"+strconv.Itoa(int(user.ID))+" was deleted",
	})
}
