package main

import (
	"gopkg.in/gin-gonic/gin.v1"
	"github.com/jinzhu/gorm"
	_"github.com/jinzhu/gorm/dialects/mysql"
	"net/http"
	"log"
)

type ToDo struct {
/*Todo struct has one field extra gorm.Model what does it mean? well
this field will embed a Model struct for us which contains four fields “ID, CreatedAt, UpdatedAt, DeletedAt*/
	gorm.Model
	Title string `json:"title"`
	Completed int `json:"completed"`
}
type TransfomedToDo struct {
	ID uint `json:"id"`
	Title string `json:"title"`
	Completed bool `json:"completed"`
}
func main()  {
	db:=Database()
	db.AutoMigrate(&ToDo{})
	gin:=gin.Default()
	gin.Use(Cors())
	v1:=gin.Group("api/v1/todos")
	{
		v1.POST("/",CreateToDo)
		v1.GET("/",FetchAllToDo)
		v1.GET("/:id",FetchSingleToDo)
		v1.PUT("/:id",UpdateToDo)
		v1.DELETE("/:id",DeleteToDo)
	}
	gin.Run(":8080")
}
func Cors() gin.HandlerFunc{
	return func(c *gin.Context) {
		c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}
func Database() *gorm.DB{
	db,err:=gorm.Open("mysql","root:123456a@@/demo?charset=utf8&parseTime=True&loc=Local")
	if err !=nil{
		panic(err)
	}
	return db
}
func CreateToDo(g *gin.Context)  {
	db:=Database()
	defer db.Close()
	var todo ToDo
	g.Bind(&todo)
	log.Println("Todo: ",todo.Title,todo.Completed)
	if todo.Title != ""{
		db.Save(&todo)
		g.JSON(http.StatusCreated, gin.H{
			"status": http.StatusCreated,
			"message":"Todo item created sucssessfully",
			"resourceId":todo.ID})
		return
	}
	g.JSON(http.StatusConflict,gin.H{
		"status": http.StatusConflict,
		"message": "Fields is empty",
	})
}
func FetchAllToDo(g *gin.Context)  {
	db:=Database()
	defer db.Close()
	var todos []ToDo
	var _todos []TransfomedToDo
	db.Find(&todos)
	if len(todos)<=0{
		g.JSON(http.StatusNotFound,gin.H{
			"status": http.StatusNotFound,
			"message": "No todo found",
		})
	}else {
		for _,item:= range todos{
			completed:=false
			if item.Completed ==1{
				completed=true
			}else{
				completed=false
			}
			_todos=append(_todos,TransfomedToDo{ID:item.ID,Completed:completed,Title:item.Title})
		}
		g.JSON(http.StatusOK,gin.H{
			"status": http.StatusOK,
			"data": _todos,
		})
	}
}
func FetchSingleToDo(g *gin.Context){
	db:=Database()
	defer db.Close()
	var todo ToDo
	todoId:=g.Params.ByName("id")
	db.First(&todo,todoId)
	if todo.ID == 0{
		g.JSON(http.StatusNotFound,gin.H{
			"status": http.StatusNotFound,
			"message": "No todo found",
		})
		return
	}
	completed:=false
	if todo.Completed == 1{
		completed=true
	}else{
		completed=false
	}
	_todo:=TransfomedToDo{ID:todo.ID,Title: todo.Title,Completed:completed}
	g.JSON(http.StatusOK,gin.H{
		"status": http.StatusOK,
		"data": _todo,
	})

}
func UpdateToDo(g *gin.Context){
	db:=Database()
	defer db.Close()
	var todo ToDo
	todoId:=g.Params.ByName("id")
	db.First(&todo,todoId)
	if todo.ID == 0{
		g.JSON(http.StatusNotFound,gin.H{
			"status": http.StatusNotFound,
			"message": "No todo found",
		})
		return
	}
	var newtodo ToDo
	g.Bind(&newtodo)
	db.Model(&todo).Update("title",newtodo.Title)
	db.Model(&todo).Update(("completed"),newtodo.Completed)
	db.Save(&todo)
	g.JSON(http.StatusOK,gin.H{
		"status": http.StatusOK,
		"message": "Todo was updated successfully",
	})
}
func DeleteToDo(g *gin.Context){
	db:=Database()
	defer db.Close()
	var todo ToDo
	todoId:=g.Params.ByName("id")
	db.First(&todo,todoId)
	if todo.ID == 0{
		g.JSON(http.StatusNotFound,gin.H{
			"status": http.StatusNotFound,
			"message": "No todo found",
		})
		return
	}
	db.Delete(&todo)
	g.JSON(http.StatusOK,gin.H{
		"status": http.StatusOK,
		"message": "Todo was deleted successfully",
	})
}
