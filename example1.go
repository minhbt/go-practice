package main

import (
	"log"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
)

type Gopher struct {
	Name string `json:"name"`
	Job string `json:"job"`
}
func (g Gopher) Map() map[string]interface{}{
	return map[string]interface{}{
		"name": g.Name,
		"job": g.Job,
	}
}
func main()  {
	gin.SetMode("release")
	g:= gin.Default()
	g.GET("/",func(c * gin.Context){
		c.JSON(http.StatusOK,gin.H{"message":"OK"})
	})
	api:=g.Group("/api")
	{
		api.GET("/gopher", ListGopher)
		api.POST("/gopher", AddGopher)
	}
	g.NoRoute(func(c *gin.Context){
		c.JSON(http.StatusNotFound,gin.H{"error":"I dont' know what you are looking for"})
	})
	log.Println("Starting server on port 8080")
	g.Run(":8080")
}
func ListGopher(c *gin.Context)  {
	var list []Gopher
	list=append(list,Gopher{"Bui Thanh Minh","Sysadmin"})
	c.JSON(http.StatusOK,gin.H{"data":list})
}
func AddGopher(c *gin.Context){
	var body struct{
		Data map[string]interface{}
	}
	if err:=c.Bind(&body);err !=nil{
		c.JSON(http.StatusConflict,gin.H{"error": err.Error()})
		return
	}
	name,_ :=body.Data["name"].(string)
	job,_ :=body.Data["job"].(string)
	if name == "" || job == ""{
		c.JSON(http.StatusConflict,gin.H{"error": "Invalid Data"})
		return
	}
	gopher:=Gopher{Name: name,Job:job}
	c.JSON(http.StatusCreated,gin.H{"data": gopher.Map()})
}
