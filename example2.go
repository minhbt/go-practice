package main

import (
	"gopkg.in/gin-gonic/gin.v1"
	"log"
	"net/http"
	"github.com/jinzhu/gorm"
	_"github.com/mattn/go-sqlite3"
)
type Users struct {
	//The “gorm” param will be used later, with the database connection
	Id int `gorm: "AUTO_INCREMENT form: "id" json:"id"`
	FirstName string `gorm: "not null" form :"id" json:"first_name"`
	LastName string `gorm: "not null form: "id" json:"last_name"`
}
var USERS = InitialUsers()
func main()  {
	gin:=gin.Default()
	gin.Use(Cors())
	InitDb()
	v1:=gin.Group("api/v1")
	{
		v1.POST("/users",PostUser)
		v1.GET("/users",GetUsers)
		v1.GET("/users/:id",GetUser)
		v1.PUT("/users/:id",UpdateUser)
		v1.DELETE("/users/:id",DeleteUser)
	}
	log.Println("Starting server on port 8080")
	gin.Run(":8080")
}
func Cors() gin.HandlerFunc{
	return func(c *gin.Context) {
		c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}
func InitDb() *gorm.DB{
	//Open file sqlite
	db,err:=gorm.Open("sqlite3","./database.db")
	if err !=nil{
		panic(err)
	}
	//Creating DB
	if !db.HasTable(&Users{}){
		db.CreateTable(&Users{})
		db.Set("gorm:table_options","ENGINE=InnoDB").CreateTable(&Users{})
	}
	return db
}
func InitialUsers() []Users {
	var users []Users
	users=append(users,Users{1,"Thanh","Minh"})
	users=append(users,Users{2,"Hoang","Huyen"})
	users=append(users,Users{3,"Minh","Quan"})
	return users
}
func PostUser(g *gin.Context)  {
	db:=InitDb()
	defer db.Close()
	var user Users
	g.Bind(&user)
	if user.FirstName != "" && user.LastName != ""{
		// INSERT INTO "users" (name) VALUES (user.Name);
		db.Create(&user)
		db.Save(&user)
		g.JSON(http.StatusOK,gin.H{"success": user})
	}else{
		g.JSON(http.StatusConflict,gin.H{"error":"Fields is empty"})
	}
// curl -i -X POST -H "Content-Type: application/json" -d "{ \"firstname\": \"Thea\", \"lastname\": \"Queen\" }" http://localhost:8080/api/v1/users
}
func GetUsers(g *gin.Context){
	db:=InitDb()
	defer db.Close()
	var users []Users
	// SELECT * FROM users
	db.Find(&users)
	g.JSON(http.StatusOK,gin.H{"data": users})
	// curl -i http://localhost:8080/api/v1/users
}
func GetUser(g *gin.Context){
	db:=InitDb()
	defer db.Close()
	id:=g.Params.ByName("id")
	var user Users
	// SELECT * FROM users WHERE id = 1;
	db.First(&user,id)
	if user.Id !=0{
		g.JSON(http.StatusOK,gin.H{"success": user})
	}else{
		g.JSON(http.StatusNotFound,gin.H{"error": "User not found"})
	}
}
func UpdateUser(g *gin.Context){
	db:=InitDb()
	defer db.Close()
	id:=g.Params.ByName("id")
	var user Users
	// SELECT * FROM users WHERE id = 1;
	db.First(&user,id)
	if user.FirstName != "" && user.LastName != ""{
		if user.Id != 0{
			var newUser Users
			g.Bind(&newUser)
			result:=Users{
				Id: user.Id,
				FirstName: newUser.FirstName,
				LastName: newUser.LastName,
			}
			// UPDATE users SET firstname='newUser.Firstname', lastname='newUser.Lastname' WHERE id = user.Id;
			db.Save(&result)
			g.JSON(http.StatusOK,gin.H{"success": result})
		}else {
			g.JSON(http.StatusNotFound,gin.H{"error":"User not found"})
		}

	}else{
		g.JSON(http.StatusConflict,gin.H{"error":"Fields is empty"})
	}
	// curl -i -X PUT -H "Content-Type: application/json" -d "{ \"firstname\": \"Thea\", \"lastname\": \"Merlyn\" }" http://localhost:8080/api/v1/users/1
}
func DeleteUser(g *gin.Context)  {
	db:=InitDb()
	defer db.Close()
	var user Users
	id:=g.Params.ByName("id")
	db.First(&user,id)
	if user.Id != 0{
		// DELETE FROM users WHERE id = user.Id
		db.Delete(&user)
		g.JSON(http.StatusOK,gin.H{"success": "User #"+id+" deleted"})
	}else{
		g.JSON(http.StatusNotFound,gin.H{"error":"User not found"})
	}
	// curl -i -X DELETE http://localhost:8080/api/v1/users/1
}
func OptionUser(g *gin.Context){
	g.Writer.Header().Set("Access-Control-Allow-Methods","DELETE,POST, PUT")
	g.Writer.Header().Set("Access-Control-Allow-Headers","Content-Type")
	g.Next()
}